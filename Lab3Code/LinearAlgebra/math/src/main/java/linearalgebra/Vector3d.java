package linearalgebra;

/*
 * Vector3d methods used to help make mathematical solutions
 * 
 * @author Yu Hua Yang 2133677
 * @version 09/09/2022
 */

public class Vector3d {

    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        return Math.sqrt((Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2) ));
    }

    public double dotProduct(Vector3d vector){
        return ((this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ()));
    }

    public Vector3d add(Vector3d vector){
        double nx = this.x + vector.getX();
        double ny = this.y + vector.getY();
        double nz = this.z + vector.getZ();

        Vector3d nVector = new Vector3d(nx, ny, nz);

        return nVector;
    }

}

