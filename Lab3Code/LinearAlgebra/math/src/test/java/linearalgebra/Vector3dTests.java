package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

/*
 * Vector3dTest used to test Vector3d's Methods
 * 
 * @author Yu Hua Yang 2133677
 * @version 09/09/2022
 */


public class Vector3dTests {
    
    @Test
    public void Vector3dTests_ConfirmGetMethodsReturnCorrectResults(){
        Vector3d vector = new Vector3d(3,2,1);
        assertEquals(3.0, vector.getX(), 0);
        assertEquals(2.0, vector.getY(), 0);
        assertEquals(1.0, vector.getZ(), 0);
    }

    @Ignore
    @Test
    public void Vector3dTests_ConfirmMagnitudeReturnCorrectResults(){
        Vector3d vector = new Vector3d(3,2,1);
        assertEquals(Math.sqrt(14), vector.magnitude(),0);
    }

    @Test
    public void Vector3dTests_ConfirmDotProductReturnCorrectResults(){
        Vector3d vector = new Vector3d(3,2,1);
        Vector3d vector2 = new Vector3d(6,5,4);
        assertEquals(32.0, vector.dotProduct(vector2), 0);
    }

    @Test
    public void Vector3dTests_ConfirmAddReturnCorrectResults(){
        Vector3d vector = new Vector3d(3,2,1);
        Vector3d vector2 = new Vector3d(6,5,4);
        Vector3d vector3 = vector.add(vector2);
        assertEquals(9.0, vector3.getX(), 0);
        assertEquals(7.0, vector3.getY(), 0);
        assertEquals(5.0, vector3.getZ(), 0);
    }


}
